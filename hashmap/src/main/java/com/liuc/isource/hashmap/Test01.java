package com.liuc.isource.hashmap;

import java.util.HashMap;

public class Test01 {

    public static void main(String[] args) {

        HashMap<Integer,String> map = new HashMap<>();
        map.put(1,"张三");
        map.put(9,"李四");
        map.put(4,"王五");
        map.put(7,"赵六");
        map.put(5,"万七");
        System.out.println(map);
    }
}
