package com.liuc.isource.hashmap;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;

public class Test02 {
    public static void main(String[] args) {
        try {
            Class<?> clazz = Class.forName("java.util.HashMap");
                try {
                    Constructor<?> c = clazz.getConstructor(int.class,int.class);
                    HashMap o2 = null;
                    try {
                        o2 = (HashMap) c.newInstance(1);

                    } catch (InstantiationException e) {
                        e.printStackTrace();
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    } catch (InvocationTargetException e) {
                        e.printStackTrace();
                    }
                    o2.put("str","hello");
                    System.out.println(o2.get("str"));
                } catch (NoSuchMethodException e) {
                    e.printStackTrace();
                }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }



    }
}
