package com.liuc.isource.hashmap;

/**
 * 测试tableSizeFor方法
 * 当在实例化HashMap实例时，如果给定了initialCapacity，由于HashMap的capacity都是2的幂，
 * 因此这个方法用于找到大于等于initialCapacity的最小的2的幂（initialCapacity如果就是2的幂，则返回的还是这个数）。
 * 根据容量参数，返回一个2的n次幂的table长度。
 * @author liuc
 */
public class TableSizeForTest {
    static final int MAXIMUM_CAPACITY = 1 << 30;
    static final int tableSizeFor(int cap) {
        int n = cap - 1;
        n |= n >>> 1;
        n |= n >>> 2;
        n |= n >>> 4;
        n |= n >>> 8;
        n |= n >>> 16;
        return (n < 0) ? 1 : (n >= MAXIMUM_CAPACITY) ? MAXIMUM_CAPACITY : n ;
    }

    public static void main(String[] args) {
        System.out.println("当定义HashMap的初始容量参数为1时，返回一个2的n次幂的table长度为："+tableSizeFor(1));
        System.out.println("当定义HashMap的初始容量参数为5时，返回一个2的n次幂的table长度为："+tableSizeFor(5));
        System.out.println("当定义HashMap的初始容量参数为25时，返回一个2的n次幂的table长度为："+tableSizeFor(25));
        System.out.println("当定义HashMap的初始容量参数为125时，返回一个2的n次幂的table长度为："+tableSizeFor(125));
        System.out.println("当定义HashMap的初始容量参数为625时，返回一个2的n次幂的table长度为："+tableSizeFor(625));
    }
}
