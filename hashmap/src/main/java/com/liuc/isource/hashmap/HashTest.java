package com.liuc.isource.hashmap;

public class HashTest {
    public static void main(String[] args) {
//        System.out.println("hash:"+hash("liu"));
        int hash1 = hash1("liu");
        System.out.println("hash1:"+hash1);
        System.out.println("如果此时的HashMap的容量是16，key为liu应放在HashMap的哪个桶里？");
        int num = hash1;
        //定义一个String的result，默认值为空
        String result = "";
        //将num放入到for循环里面，每次循环结束都除以2
        for(int i = num; i > 0; i/=2){
            //将i的数值对2取余，然后进行字符串的拼接，最后赋值给result
            result = i % 2 + result;
        }
        System.out.println("hash1的二进制是："+result);
        int num1 = (16 - 1);
        //定义一个String的result，默认值为空
        String result1 = "";
        //将num放入到for循环里面，每次循环结束都除以2
        for(int i = num1; i > 0; i/=2){
            //将i的数值对2取余，然后进行字符串的拼接，最后赋值给result
            result1 = i % 2 + result1;
        }
        System.out.println("HashMap的容量是16,此时进行计算（16-1）的二进制是："+result1);
        int  i = (16 - 1) & hash1;
        System.out.println("如果此时的HashMap的容量是16，key为liu应放在 "+i+" 号桶里");
    }

    static final int hash(Object key) {
        int h;
        return (key == null) ? 0 : (h = key.hashCode()) ^ (h >>> 16);
    }

    static final int hash1(Object key) {
        int h;
        if (key == null) {
           h = 0;
        }
        h = key.hashCode();
        System.out.println("key.hashCode():"+h);
        //将扫描器输入的值赋值给int类型的num
        int num = h;
        //定义一个String的result，默认值为空
        String result = "";
        //将num放入到for循环里面，每次循环结束都除以2
        for(int i = num; i > 0; i/=2){
            //将i的数值对2取余，然后进行字符串的拼接，最后赋值给result
            result = i % 2 + result;
        }
        System.out.println("key.hashCode()的二进制是："+result);
        System.out.println("h >>> 16:"+(h >>> 16));
        int num1 = h >>> 16;
        //定义一个String的result，默认值为空
        String result1 = "";
        //将num放入到for循环里面，每次循环结束都除以                      2
        for(int i = num1; i > 0; i/=2){
            //将i的数值对2取余，然后进行字符串的拼接，最后赋值给result
            result1 = i % 2 + result1;
        }
        System.out.println("key.hashCode() >>> 16的二进制是:"+result1);
        h = (h) ^ (h >>> 16);
        int num2 = h ;
        //定义一个String的result，默认值为空
        String result2 = "";
        //将num放入到for循环里面，每次循环结束都除以2
        for(int i = num2; i > 0; i/=2){
            //将i的数值对2取余，然后进行字符串的拼接，最后赋值给result
            result2 = i % 2 + result2;
        }
        System.out.println("key.hashCode 与key.hashCode() >>> 16进行异或运算得到的结果的二进制是:"+result2);
        System.out.println(h);
        return h;
    }
}
